﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    private GameObject screenshotter;
    private GameObject player;
    private GameObject guiInGame;
    private GameObject guiAfterGame;
    private int timeToPlay = 0;
    private float timeLeft = 0;
    [SerializeField]
    private Text timerText;

    void Start () {
        this.timeToPlay = GameInfo.SecondsToPlay;
        this.timeLeft = this.timeToPlay;

        this.player = GameObject.FindGameObjectWithTag("Player");

        this.guiInGame = GameObject.FindGameObjectWithTag("InGameGUI");

        bool active = (timeToPlay == 0);
        this.guiInGame.SetActive(true);
        GameObject.FindGameObjectWithTag("EndGame").SetActive(active);
        GameObject.FindGameObjectWithTag("Timer").SetActive(!active);

        this.guiAfterGame = GameObject.FindGameObjectWithTag("AfterGameGUI");
        this.guiAfterGame.SetActive(false);

        this.screenshotter = GameObject.FindGameObjectWithTag("Screenshotter");
        this.screenshotter.SetActive(false);
    } // end Start

    void Update() {
        if (this.timeToPlay != 0) {
            this.timeLeft -= Time.deltaTime;
            this.timerText.text = GameInfo.ToTime(this.timeLeft);

            if (this.timeLeft <= 0) {
                this.EndGame();
            }
        }
    } // end Update

    public void EndGame() {
        this.player.SetActive(false);
        this.guiInGame.SetActive(false);
        this.guiAfterGame.SetActive(true);
        this.screenshotter.SetActive(true);
        this.screenshotter.GetComponent<Screenshotter>().Init();
    } // end EndGame

    public void BackToStart() {
        SceneManager.LoadScene("Menu"); // eigentlich Hauptmenü
    } // end BackToStart

} // end GameManager
