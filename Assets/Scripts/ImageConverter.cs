﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

/**
 * Klasse wandelt ein gegebenes .bytes file in ein Terrain um.
 * */
public class ImageConverter : MonoBehaviour {

    private int fileSize;
    private int rootedFileSize;
    [SerializeField]
    private float terrainheight;
    private TextAsset imgAsset;

	// Use this for initialization
	void Start () {
        this.imgAsset = Resources.Load("Pictures/example2.jpg") as TextAsset;
        this.ByteArrayToTerrain(this.ProcessImage());
    }

    private byte[] ByteInterpolation(byte[] input) {
        Debug.Log("size of input array: " + input.Length);

        // neues byte array erstellen mit Länge des inputs + die Anzahl aller Interpolationsbytes
        byte[] output = new byte[input.Length + (input.Length / 4)];
        // input array wird neu sortiert, damit eine zufällige Verteilung stattfindet
        //byte[] resortedInput = this.Resort(input);
        byte[] resortedInput = input;
        List<byte> sortedByteList = resortedInput.OfType<byte>().ToList();
        List<byte> outputList = new List<byte>();
        int position = 0;
        // Output Liste in 4er Paketen mit Inhalt der Inputliste füllen und nach einem 4er Paket an Interpolationsbyte anhängen, welches sich aus
        // dem letzten bytes des aktuellen Paketes ergibt und aus dem ersten Byte des nächsten Paketes, momentan noch in der Mitte der Interpolationsgerade
        // also quasi der Durchschnitt beider Werte
        foreach (byte b in sortedByteList) {
            position += 4;
            if (position <= sortedByteList.Count) { 
                outputList.Add(sortedByteList[position - 4]);
                outputList.Add(sortedByteList[position - 3]);
                outputList.Add(sortedByteList[position - 2]);
                outputList.Add(sortedByteList[position - 1]);
                // Interpolationsbyte muss nicht mehr angehangen werden, wenn der letzte Block sortiert wird.
                if (position != sortedByteList.Count) {
                    int thisBlockLastByte = sortedByteList[position - 1];
                    int nextBlockFirstByte = sortedByteList[position];
                    byte interpolatedByte = (byte)Math.Ceiling((decimal)(thisBlockLastByte + nextBlockFirstByte) / 2);
                    outputList.Add(interpolatedByte);
                }
            }
        }
        Debug.Log("size of output array: " + outputList.ToArray().Length);
        return outputList.ToArray();
    }

    private byte[] Resort(byte[] input) {
        byte[] output = new byte[input.Length];
        int temp = 0;
        // TODO: Hässlicher Code. Der Resort der 4er Pakete muss am besten mit Schleife geregelt werden.
        try {
            for (int i = 0; i < input.Length; i += 8) {
                temp = i;
                byte[] package = new byte[4];
                package[0] = input[i];
                package[1] = input[i + 1];
                package[2] = input[i + 2];
                package[3] = input[i + 3];
                output[i] = input[i + 4];
                output[i + 1] = input[i + 5];
                output[i + 2] = input[i + 6];
                output[i + 3] = input[i + 7];
                output[i + 4] = package[0];
                output[i + 5] = package[1];
                output[i + 6] = package[2];
                output[i + 7] = package[3];
            }
        } catch (Exception e) {
            Debug.Log("Array index was " + temp + " but array size only was " + input.Length);
            return output;
        }
        return output;
    }

    private void ByteArrayToTerrain(byte[] input) {
        float[,] map = new float[this.rootedFileSize, this.rootedFileSize];
        int value = 0;
        for (int i = 0; i < input.Length; i++) {
            if (input[i] >= value) {
                value = input[i];
            }
        }
        int counter = 0;
        for (int x = 0; x < this.rootedFileSize; x++) {
            for (int y = 0; y < this.rootedFileSize; y++) {
                float terrainHeigth = input[counter] / (value * 0.9f);
                map[x, y] = terrainHeigth;
                //Debug.Log(x + " " + y + " " + map[x, y]);
                counter++;
            }
        }

        Terrain aTerrain = FindObjectOfType<Terrain>();
        aTerrain.terrainData.heightmapResolution = this.rootedFileSize;
        aTerrain.terrainData.size = new Vector3(this.rootedFileSize, this.terrainheight, this.rootedFileSize);
        aTerrain.terrainData.SetHeights(0, 0, map);
    }

    private byte[] ProcessImage() {
        Texture2D tex = new Texture2D(0, 0);
        tex.LoadImage(this.imgAsset.bytes);
        tex.Compress(false);
        byte[] imgData = tex.GetRawTextureData();
        this.fileSize = imgData.Length;
        this.rootedFileSize = (int)Math.Floor(Math.Sqrt(this.fileSize));
        if(this.rootedFileSize > 257) {
            this.rootedFileSize = 257;
        }
        return this.ByteInterpolation(imgData);
    }
}
