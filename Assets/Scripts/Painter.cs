﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enums;

public class Painter : MonoBehaviour {

    [SerializeField]
    private Texture2D brush;
    private Color[] brushPixels = null;
    private TerrainData terrainData;
    private float[,,] splatmap;
    private Vector3 prevPos = Vector3.zero;

    [SerializeField]
    private float terrainWidth = 100f;
    [SerializeField]
    private float terrainHeight = 100f;

    private float[,] heights;
    private float minHeight = 0f;
    private float maxHeight = 0f;

    public int stopAfter = 1;
    private int c = 0;

    private int halfBrushWidth;
    private int halfBrushHeight;
    
    private TerrainColor terrainColorTop;
    private TerrainColor terrainColorBot;

    private float scaleFactor;

    public void Init () {
        this.terrainData = Terrain.activeTerrain.terrainData;
        this.brushPixels = this.brush.GetPixels();

        this.splatmap = new float[
            this.terrainData.alphamapWidth,
            this.terrainData.alphamapHeight,
            this.terrainData.alphamapLayers
        ];

        // "Basisfarbe auftragen"
        for (var y = 0; y < this.terrainData.alphamapHeight; y++) {
            for (var x = 0; x < this.terrainData.alphamapWidth; x++) {
                this.splatmap[x, y, 0] = 1f;
            }
        }
        this.terrainData.SetAlphamaps(0, 0, this.splatmap);

        // Min/Max Höhen ermitteln
        this.heights = this.terrainData.GetHeights(0, 0, this.terrainData.heightmapWidth, this.terrainData.heightmapHeight);

        this.minHeight = 0f;
        this.maxHeight = 0f;
        for (int i = 0; i < this.terrainData.heightmapWidth; i++) {
            for (int j = 0; j < this.terrainData.heightmapHeight; j++) {
                float height = this.heights[i, j];
                if (height > this.maxHeight) this.maxHeight = height;
                if (height < this.minHeight) this.minHeight = height;
            }
        }

        this.halfBrushWidth = (this.brush.width / 2);
        this.halfBrushHeight = (this.brush.height/ 2);

        this.SetTerrainColor(GameInfo.TerrainColor);

        this.scaleFactor = this.terrainData.heightmapResolution / this.terrainData.alphamapResolution;
    } // end Init

    void FixedUpdate () {
        Vector3 posOnTerrain = this.PlayerPosToTerrainPos(this.transform.position);

        float movedDistance = (posOnTerrain - this.prevPos).magnitude;

        if (movedDistance > 1f) {
            this.PaintWithBrush(posOnTerrain.z, posOnTerrain.x);
            this.prevPos = posOnTerrain;
        }
    } // end Update

    private void PaintWithBrush(float _x, float _z) {
        if (this.brushPixels == null || this.brushPixels.Length <= 0) return;

        int x = Mathf.RoundToInt(_x);
        int z = Mathf.RoundToInt(_z);

        int fromX = x - this.halfBrushWidth;
        int toX = x + this.halfBrushWidth;
        int fromZ = z - this.halfBrushHeight;
        int toZ = z + this.halfBrushHeight;

        int counter = 0;
        for (var currentZ = fromZ; currentZ < toZ; currentZ++) {
            for (var currentX = fromX; currentX < toX; currentX++) {
                if (currentX >= 0 && currentX < this.terrainData.alphamapWidth && currentZ >= 0 && currentZ < this.terrainData.alphamapHeight) {
                    float alphaToApply = this.brushPixels[counter].a;
                    if (alphaToApply < 0.04f) {
                        counter++;
                        continue;
                    }
                    
                    float heightPercentage = this.GetHeightPercentage(this.heights[Mathf.RoundToInt(currentX * this.scaleFactor), Mathf.RoundToInt(currentZ * this.scaleFactor)]);
                    
                    // In Verhältnis aufsplitten und somit Farbe "mischen" (in voller Intensität)
                    float colorTop = heightPercentage;
                    float colorBot = (1f - heightPercentage);
                    float white = 0f;

                    // Alpha anpassen
                    float aWhiten = (1f - alphaToApply);
                    aWhiten = aWhiten / 2;

                    // colorTop
                    float currentAlphaTop = this.splatmap[currentX, currentZ, (int) this.terrainColorTop];
                    float modColorTop = colorTop + currentAlphaTop - aWhiten;
                    modColorTop = Mathf.Clamp(modColorTop, 0f, colorTop);
                    this.splatmap[currentX, currentZ, (int)this.terrainColorTop] = modColorTop;

                    // colorBottom
                    float currentAlphaBot = this.splatmap[currentX, currentZ, (int) this.terrainColorBot];
                    float modColorBot = colorBot + currentAlphaBot - aWhiten;
                    modColorBot = Mathf.Clamp(modColorBot, 0f, colorBot);
                    this.splatmap[currentX, currentZ, (int)this.terrainColorBot] = modColorBot;
                    
                    // white
                    white = 1f - modColorTop - modColorBot;
                    this.splatmap[currentX, currentZ, 0] = white;
                    counter++;
                }
            }
        }

        this.terrainData.SetAlphamaps(0, 0, this.splatmap);
    } // end PaintWithBrush

    /**
     * Ermittelt die Position des Spielers auf dem Terrain
     */
    private Vector3 PlayerPosToTerrainPos(Vector3 playerPos) {
        float xPercent = ((100f / this.terrainWidth) * playerPos.x);
        float zPercent = ((100f / this.terrainHeight) * playerPos.z);

        int x = Mathf.RoundToInt((this.terrainData.alphamapWidth / 100f) * xPercent);
        int y = 0;
        int z = Mathf.RoundToInt((this.terrainData.alphamapHeight / 100f) * zPercent);

        return new Vector3(x, y, z);
    } // end PlayerPosToTerrainPos

    /// <summary>
    /// Returnt einen Wert zwischen 0 (Boden) und 1 (höchster Punkt in Terrain).
    /// </summary>
    /// <param name="heightValue">Ein Höhenwert</param>
    /// <returns>Wert zwischen 0 (Boden) und 1 (höchster Punkt in Terrain)</returns>
    private float GetHeightPercentage(float heightValue) {
        float heightPercentage = (100 / this.maxHeight * heightValue);
        return Mathf.Clamp01(heightPercentage/100);
    }

    private void SetTerrainColor(ColorPreset color) {
        switch (color) {
            case ColorPreset.Red:
                this.terrainColorTop = TerrainColor.RedTop;
                this.terrainColorBot = TerrainColor.RedBot;
                break;
            case ColorPreset.Blue:
                this.terrainColorTop = TerrainColor.BlueTop;
                this.terrainColorBot = TerrainColor.BlueBot;
                break;
            case ColorPreset.Green:
                this.terrainColorTop = TerrainColor.GreenTop;
                this.terrainColorBot = TerrainColor.GreenBot;
                break;
            case ColorPreset.Pink:
                this.terrainColorTop = TerrainColor.PinkTop;
                this.terrainColorBot = TerrainColor.PinkBot;
                break;
            default:
                this.terrainColorTop = TerrainColor.RedTop;
                this.terrainColorBot = TerrainColor.RedBot;
                break;
        }
        
    } // end SetTerrainColor

} // end Painter
