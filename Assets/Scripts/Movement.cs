﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour {

    [SerializeField]
    private Slider sliderSpeed;
    [SerializeField]
    private Slider sliderJump;

    [SerializeField]
    private float tilt = 0.8f; // 0 = flach auf Tisch und 1 = vertikal vor Einem
    public Vector3 forwards;
    private Vector3 force = Vector3.zero;

    private Rigidbody rigid;
    [SerializeField]
    private float accl = 0.8f;
    [SerializeField]
    private float brk = 2.5f;
    [SerializeField]
    private float rotationSpeed = 0.2f;

    [SerializeField]
    private float currentSpeed = 0f;
    [SerializeField]
    private float minSpeed = 0f;
    [SerializeField]
    private float maxSpeed = 130f;

    [SerializeField]
    private float maxJumpHoldTime = 2f;
    private float jumpHoldTime = 0f;
    private bool rise = true;
    private float jumpSpeed = 0f;
    private bool doJump = false;

    void Start () {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        rigid = GetComponent<Rigidbody>();
        this.forwards = this.transform.forward;

        this.sliderSpeed.minValue = this.minSpeed;
        this.sliderSpeed.maxValue = this.maxSpeed;

        this.sliderJump.minValue = 0f;
        this.sliderJump.maxValue = this.maxJumpHoldTime;
    }

    private void Update() {
        
    } // end Update

    void FixedUpdate() {
        if (Input.touchCount > 0) {
            if (this.rise) {
                this.jumpHoldTime += Input.GetTouch(0).deltaTime;
            }
            else {
                this.jumpHoldTime -= Input.GetTouch(0).deltaTime;
            }

            this.jumpHoldTime = Mathf.Clamp(this.jumpHoldTime, 0f, this.maxJumpHoldTime);

            if (this.rise && this.jumpHoldTime >= this.maxJumpHoldTime) {
                this.rise = false;
            }
            else if (!this.rise && this.jumpHoldTime <= 0) {
                this.rise = true;
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended) {
                //Debug.Log("Jump! => " + this.jumpHoldTime);
                this.jumpSpeed = this.jumpHoldTime * 1.7f;
                this.jumpHoldTime = 0f;
                this.doJump = true;
            }

            this.sliderJump.value = this.jumpHoldTime;

        }


        float accY = Input.acceleration.y + this.tilt;
        if (accY > 0.2f) {
            this.currentSpeed += (this.accl * Mathf.Pow(accY * 2.3f, 2)); // beschleunigen abhängig von Winkel
        } else if (Input.acceleration.y + this.tilt < -0.1f) {
            this.currentSpeed -= this.brk; // bremsen
        }

        float turnInput = 0f;
        if (Mathf.Abs(Input.acceleration.x) > 0.2f) {
            turnInput = Input.acceleration.x;
            turnInput *= this.rotationSpeed;
        }

        this.currentSpeed = Mathf.Clamp(this.currentSpeed, this.minSpeed, this.maxSpeed);
        this.sliderSpeed.value = this.currentSpeed;

        Vector3 turn = Vector3.Cross(-this.forwards, Vector3.up).normalized; // Dreh-/Lenkvektor berechnen
        turn = turn * turnInput; // Drehgeschwindigkeit anwenden

        this.forwards += turn; // Vorwärtsvektor aktualisieren (Drehbewegung anwenden)
        this.force = this.forwards;

        if (this.doJump) {
            this.rigid.AddForce(this.jumpSpeed * Vector3.up, ForceMode.Impulse);
            //this.rigid.velocity += this.jumpSpeed * Vector3.up;
            this.doJump = false;
            this.jumpSpeed = 0f;
        }

        this.force.Normalize();
        this.forwards.Normalize();
        turn.Normalize();

        

        Debug.DrawRay(this.transform.position, this.forwards * 10, Color.green);
        Debug.DrawRay(this.transform.position, this.force * 2, Color.red);
        Debug.DrawRay(this.transform.position, turn * 2, Color.blue);

        this.rigid.AddForce(this.force * this.currentSpeed * Time.deltaTime);
    } // end FixedUpdate

}
