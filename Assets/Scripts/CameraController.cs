﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    
    [SerializeField]
    private Transform target;
    private Movement movement;
    private Rigidbody targetRigid;

    [SerializeField]
    private float height = 2f;
    [SerializeField]
    private float distance = 2f;

    private float xAngle;
    private float yAngle;
    
    [SerializeField]
    private float homingSpeed = 0.1f;

    void Start () {
        this.movement = this.target.GetComponent<Movement>();
        this.targetRigid = this.target.GetComponent<Rigidbody>();
    }
	
	void LateUpdate () {
        // Position hinter der Kugel ermitteln
        Vector3 behind = -this.movement.forwards;
        behind = behind * distance;
        behind.y = this.height;

        Debug.DrawRay(this.target.position, behind, Color.cyan);

        // je schneller die Kugel, desto weiter weg ist die Kamera
        float speedOffset = this.movement.forwards.magnitude;

        Vector3 desiredPosition = this.target.position + behind * speedOffset;
        Vector3 smoothedPosition = Vector3.Lerp(this.transform.position, desiredPosition, this.homingSpeed);

        // Position und Rotation setzen
        this.transform.position = smoothedPosition;
        this.transform.LookAt(this.target.position);
    }
}
