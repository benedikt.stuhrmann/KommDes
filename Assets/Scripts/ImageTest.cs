﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ImageTest : MonoBehaviour {

    private Texture2D image;

	void Start () {
        byte[] fileData;
        string filePath = GameInfo.PathToPlayingImage;

        if (File.Exists(filePath)) {
            char[] sep = { '/', '\\', '.' };
            string[] filePathParts = filePath.Split(sep);

            fileData = File.ReadAllBytes(filePath);

            this.image = new Texture2D(2, 2);
            this.image.LoadImage(fileData);
            this.image.name = filePathParts[filePathParts.Length - 2]; // nach Namen des Bildes benennen
        } else {
            Debug.Log("Ausgewähltes Bild wurde nicht gefunden!");
        }

        this.image = this.ConvertToGrayscale(this.image);

        // Wenn das Bild hochkant ist => drehen
        if (this.image.width > this.image.height) {
            this.image = this.RotateTexture(this.image);
            this.image = this.RotateTexture(this.image);
            this.image = this.RotateTexture(this.image);
        }
        
        this.ToTerrain(this.image);

        GameInfo.Image = this.image;

        // World Border richtig setzen
        Transform worldBorderTop = GameObject.FindGameObjectWithTag("WorldBorderTop").transform;
        Transform worldBorderRight = GameObject.FindGameObjectWithTag("WorldBorderRight").transform;
        Transform worldBorderBottom = GameObject.FindGameObjectWithTag("WorldBorderBottom").transform;
        Transform worldBorderLeft = GameObject.FindGameObjectWithTag("WorldBorderLeft").transform;

        TerrainData terrainData = Terrain.activeTerrain.terrainData;
        float scaleFactor = terrainData.size.x / terrainData.heightmapResolution;

        worldBorderTop.position = new Vector3((this.image.width / 2) * scaleFactor, worldBorderTop.position.y, this.image.width * scaleFactor + 0.5f);
        worldBorderRight.position = new Vector3(this.image.height * scaleFactor + 0.5f, worldBorderRight.position.y, (this.image.height / 2) * scaleFactor);
        worldBorderBottom.position = new Vector3((this.image.width / 2) * scaleFactor, worldBorderBottom.position.y, -0.5f);
        worldBorderLeft.position = new Vector3(-0.5f, worldBorderLeft.position.y, this.image.width * scaleFactor);

        GameObject.FindObjectOfType<Painter>().Init();
    } // end Start
    
    private Texture2D ConvertToGrayscale(Texture2D img) {
        Color32[] pixels = img.GetPixels32();

        for (int x = 0; x < img.width; x++) {
            for (int y = 0; y < img.height; y++) {
                Color32 pixel = pixels[x + y * img.width];
                int p = ((256 * 256 + pixel.r) * 256 + pixel.b) * 256 + pixel.g;
                int b = p % 256;
                p = Mathf.FloorToInt(p / 256);
                int g = p % 256;
                p = Mathf.FloorToInt(p / 256);
                int r = p % 256;
                float l = (0.2126f * r / 255f) + 0.7152f * (g / 255f) + 0.0722f * (b / 255f);
                Color c = new Color(l, l, l, 1);
                img.SetPixel(x, y, c);
            }
        }
        return img;
    } // end ConvertToGrayscale

    private Texture2D RotateTexture(Texture2D image) {
        Texture2D rotImage = new Texture2D(image.height, image.width, image.format, false);

        Color32[] pixels = image.GetPixels32(0);
        pixels = this.RotateTextureGrid(pixels, image.width, image.height);
        rotImage.SetPixels32(pixels);
        rotImage.Apply();

        return rotImage;
    } // end RotateTexture


    private Color32[] RotateTextureGrid(Color32[] tex, int wid, int hi) {
        Color32[] ret = new Color32[wid * hi];

        for (int y = 0; y < hi; y++) {
            for (int x = 0; x < wid; x++) {
                ret[(hi - 1) - y + x * hi] = tex[x + y * wid];

            }
        }

        return ret;
    } // end RotateTextureGrid

    private void ToTerrain(Texture2D img) {
        Color[] pixels = img.GetPixels();

        float[,] map = new float[img.width, img.height];

        int counter = 0;
        float maxHeight = 0f;
        for (int y = 0; y < img.height; y++) {
            for (int x = 0; x < img.width; x++) {
                Color pixel = pixels[counter];
                counter++;

                float height = pixel.r + pixel.g + pixel.b;
                if (height > maxHeight) maxHeight = height;

                map[x, y] = height;
            }
        }

        map = this.FlatenOutMap(map);

        TerrainData terrainData = Terrain.activeTerrain.terrainData;
        terrainData.heightmapResolution = Mathf.Max(img.height, img.width);
        terrainData.size = new Vector3(100, 2, 100);
        terrainData.SetHeights(0, 0, map);
    } // end ToTerrain

    private float[,] FlatenOutMap(float[,] oldMap) {
        int mapSizeX = oldMap.GetLength(0);
        int mapSizeY = oldMap.GetLength(1);
        float threshold = 0.1f;
        float[,] newMap = new float[mapSizeX, mapSizeY];
        for (int x = 0; x < mapSizeX; x++) {
            for (int y = 0; y < mapSizeY; y++) {
                float thisPxl = oldMap[x, y];
                newMap[x, y] = oldMap[x, y];
                float tl = 10000;
                if (x - 1 >= 0 && y + 1 < mapSizeY) {
                    tl = oldMap[x - 1, y + 1];
                }
                float tm = 10000;
                if (y + 1 < mapSizeY) {
                    tm = oldMap[x, y + 1];
                }
                float tr = 10000;
                if (x + 1 < mapSizeX && y + 1 < mapSizeY) {
                    tr = oldMap[x + 1, y + 1];
                }
                float l = 10000;
                if (x - 1 >= 0) {
                    l = oldMap[x - 1, y];
                }
                float r = 10000;
                if (x + 1 < mapSizeX) {
                    r = oldMap[x + 1, y];
                }
                float bl = 10000;
                if (x - 1 >= 0 && y - 1 >= 0) {
                    bl = oldMap[x - 1, y - 1];
                }
                float bm = 10000;
                if (y - 1 >= 0) {
                    bm = oldMap[x, y - 1];
                }
                float br = 10000;
                if (x + 1 < mapSizeX && y - 1 >= 0) {
                    br = oldMap[x + 1, y - 1];
                }

                int divisor = 8;
                if (tl != 10000 && Mathf.Abs(thisPxl - tl) > threshold) {
                    newMap[x - 1, y + 1] = newMap[x - 1, y + 1] + (Mathf.Abs(thisPxl - tl) / divisor);
                }
                if (tm != 10000 && Mathf.Abs(thisPxl - tm) > threshold) {
                    newMap[x, y + 1] = newMap[x, y + 1] + (Mathf.Abs(thisPxl - tm) / divisor);
                }
                if (tr != 10000 && Mathf.Abs(thisPxl - tr) > threshold) {
                    newMap[x + 1, y + 1] = newMap[x + 1, y + 1] + (Mathf.Abs(thisPxl - tr) / divisor);
                }
                if (l != 10000 && Mathf.Abs(thisPxl - l) > threshold) {
                    newMap[x - 1, y] = newMap[x - 1, y] + (Mathf.Abs(thisPxl - l) / divisor);
                }
                if (r != 10000 && Mathf.Abs(thisPxl - r) > threshold) {
                    newMap[x + 1, y] = newMap[x + 1, y] + (Mathf.Abs(thisPxl - r) / divisor);
                }
                if (bl != 10000 && Mathf.Abs(thisPxl - bl) > threshold) {
                    newMap[x - 1, y - 1] = newMap[x - 1, y - 1] + (Mathf.Abs(thisPxl - bl) / divisor);
                }
                if (bm != 10000 && Mathf.Abs(thisPxl - bm) > threshold) {
                    newMap[x, y - 1] = newMap[x, y - 1] + (Mathf.Abs(thisPxl - bm) / divisor);
                }
                if (br != 10000 && Mathf.Abs(thisPxl - br) > threshold) {
                    newMap[x + 1, y - 1] = newMap[x + 1, y - 1] + (Mathf.Abs(thisPxl - br) / divisor);
                }

            }
        }
        return newMap;
    } // end FlatenOutMap

} // end ImageTest
