﻿using UnityEngine;
using Enums;

public static class GameInfo {

    public static int SecondsToPlay { get; set; }

    public static ColorPreset TerrainColor { get; set; }

    public static string PathToPlayingImage { get; set; }

    public static Texture2D Image { get; set; }

    public static string ToTime(float time) {
        int seconds = Mathf.RoundToInt(time);
        int minutes = seconds / 60;
        seconds = seconds % 60;

        string sec;
        if (seconds == 0) {
            sec = "00";
        }
        else if (seconds > 0 && seconds < 10) {
            sec = "0" + seconds.ToString();
        }
        else {
            sec = seconds.ToString();
        }

        if (minutes == 0 && seconds == 0)
            return "Kein Limit";
        else
            return minutes + ":" + sec + "min";
    }
}