﻿namespace Enums {
    public enum ColorPreset : int {
        Red,
        Blue,
        Green,
        Pink
    }

    public enum TerrainColor : int {
        Base,
        RedBot,
        RedTop,
        BlueBot,
        BlueTop,
        GreenBot,
        GreenTop,
        PinkBot,
        PinkTop
    }
}