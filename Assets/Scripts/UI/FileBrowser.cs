﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FileBrowser {

    public string[] GetFiles(string folder, string type) {
        #if !UNITY_ANDROID
        DirectoryInfo dirInfo = new DirectoryInfo(folder);
        FileInfo[] files = dirInfo.GetFiles(type);
        string[] fileNames = new string[files.Length];
        for (int i = 0; i < files.Length; i++) {
            fileNames[i] = folder + files[i].Name;
        }
        #endif
        #if UNITY_ANDROID
        DirectoryInfo dirInfo = new DirectoryInfo(Application.persistentDataPath);
        FileInfo[] files = dirInfo.GetFiles(type);
        string[] fileNames = new string[files.Length];
        for (int i = 0; i < files.Length; i++) {
            fileNames[i] = folder + files[i].Name;
        }

        return fileNames;
        #endif
    }

    public string GetFullFilePath(string file) {
        return Application.persistentDataPath + "/" + file;
    }
}
