﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelfEvent : MonoBehaviour {

    public void Cstm_OnFileBtnClick() {
        Text textC = this.GetComponentInChildren<Text>();
        MenuHandler mh = FindObjectOfType<MenuHandler>();
        if (textC != null) {
            FileBrowser fb = new FileBrowser();
            Debug.Log("Selected file: " + fb.GetFullFilePath(textC.text));
            GameInfo.PathToPlayingImage = fb.GetFullFilePath(textC.text);
            mh.BrowserToSettings();
        } else {
            Debug.Log("Clicked was null");
        }
    }

}
