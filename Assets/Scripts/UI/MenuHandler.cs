﻿using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Enums;

public class MenuHandler : MonoBehaviour {

    [SerializeField]
    private GameObject fileBtn;
    [SerializeField]
    private GameObject canvas;
    private int offset = 50;
    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private GameObject browserMenu;
    [SerializeField]
    private GameObject settingsMenu;
    [SerializeField]
    private Image loadingIcon;
    private bool filesLoaded;

    [SerializeField]
    private Text timeText;
    [SerializeField]
    private Slider timeSlider;
    [SerializeField]
    private Slider colorSlider;


    // Use this for initialization
    void Start() {
        this.ChangeTime();
    }

    private void Update() {
        if(!this.filesLoaded && this.browserMenu.activeSelf) {
            StartCoroutine("FillLoadingIcon");
        } else if(this.loadingIcon.fillAmount != 0) {
            this.loadingIcon.fillAmount = 0;
        }
    }

    public void OnStart() {
        this.mainMenu.SetActive(false);
        this.browserMenu.SetActive(true);
        FileBrowser fb = new FileBrowser();
        string[] filesInFolder = fb.GetFiles("", "*.jpg");
        if (!this.filesLoaded) {
            this.RenderFiles(filesInFolder);
        }
    }

    private void RenderFiles(string[] files) {
        foreach(string file in files) {
            GameObject go = Instantiate(fileBtn, new Vector3(Screen.width / 2, (Screen.height / 1.2f) - offset, 0), Quaternion.identity, GameObject.FindGameObjectWithTag("FBContent").transform);
            string pattern = "\\\\\\\\";
            string replacement = "\\";
            Regex rgx = new Regex(pattern);
            string result = rgx.Replace(file, replacement);
            go.GetComponentInChildren<Text>().text = "" + result;
            this.offset += Screen.height / 6;
            this.filesLoaded = true;
        }
    }

    private void LoadScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
        Debug.Log("Scene loaded: " + sceneName);
    }

    public void ReloadFiles() {
        StartCoroutine("DoReload");
    }

    // Nur für visuelles Feedback, damit User versteht, dass die Files auch wirklich neu geladen wurden
    public IEnumerator DoReload() {
        foreach (Transform go in GameObject.FindGameObjectWithTag("FBContent").transform) {
            Destroy(go.gameObject);
        }
        this.filesLoaded = false;
        FileBrowser fb = new FileBrowser();
        string[] filesInFolder = fb.GetFiles("", "*.txt");
        this.offset = 50;
        yield return new WaitForSeconds(0.5f);
        this.RenderFiles(filesInFolder);
    }

    // Ebenfalls nur visuelles Feedback
    private IEnumerator FillLoadingIcon() {
        this.loadingIcon.fillAmount += 0.04f;
        yield return new WaitForSeconds(.1f);
    }

    public void BrowserToSettings() {
        this.browserMenu.SetActive(false);
        this.settingsMenu.SetActive(true);
    }

    public void SettingsToBrowser() {
        this.browserMenu.SetActive(true);
        this.settingsMenu.SetActive(false);
        StartCoroutine("DoReload");
    }

    public void BrowserToMain() {
        this.mainMenu.SetActive(true);
        this.browserMenu.SetActive(false);
    }

    public void SettingsToMain() {
        this.mainMenu.SetActive(true);
        this.settingsMenu.SetActive(false);
    }

    public void ChangeTime() {
        this.timeText.text = GameInfo.ToTime(this.timeSlider.value);
    }

    public void ChangeColor() {
        ColorPreset color = ColorPreset.Red;

        switch ((int)this.colorSlider.value) {
            case 0:
                color = ColorPreset.Red;
                break;
            case 1:
                color = ColorPreset.Pink;
                break;
            case 2:
                color = ColorPreset.Blue;
                break;
            case 3:
                color = ColorPreset.Green;
                break;
            case 4:
                color = ColorPreset.Red;
                break;
            case 5:
                color = ColorPreset.Red;
                break;
        }
        GameInfo.TerrainColor = color;
    }

    public void StartGame() {
        GameInfo.SecondsToPlay = (int)this.timeSlider.value;
        SceneManager.LoadScene("Ball Rolling");
    }
}
