﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Screenshotter : MonoBehaviour {

    private Camera cam;
    private TerrainData terrainData;
    [SerializeField]
    private Slider slider;

    private float[,] savedHeights;

	public void Init () {
        this.cam = this.GetComponent<Camera>();
        this.terrainData = Terrain.activeTerrain.terrainData;

        float terrainSize = this.terrainData.bounds.size.x;
        
        this.transform.rotation = Quaternion.Euler(90, 0, 0);
        this.cam.rect = new Rect(0, 0, 1, 1);

        float terrainHeight = this.terrainData.bounds.max.y;
        this.slider.minValue = terrainHeight + 3f;
        this.slider.maxValue = terrainHeight + 50;

        this.savedHeights = this.terrainData.GetHeights(0, 0, this.terrainData.heightmapWidth, this.terrainData.heightmapHeight);

        this.SetPosition();
    } // end Start

    public void UpdatePosition () {
        float value = this.slider.value;

        if (value == this.slider.maxValue) {
            this.SetTerrainHeights();
        } else {
            this.SetTerrainHeights(false);
            this.transform.position = new Vector3(this.transform.position.x, value, this.transform.position.z);
        }
    } // end UpdatePosition

    public void TakeScreenshot() {
        StartCoroutine(TakeScreenshot(0.1f));
    } // end TakeScreenshot

    private IEnumerator TakeScreenshot(float timeToWait) {
        Canvas[] ui = GameObject.FindObjectsOfType<Canvas>();

        foreach (Canvas canvas in ui) {
            canvas.gameObject.SetActive(false);
        }

        ScreenCapture.CaptureScreenshot(System.DateTime.Now.ToString("PYE-MM-dd-yy_hh-mm-ss") + ".png");

        yield return new WaitForSeconds(timeToWait);

        foreach (Canvas canvas in ui) {
            canvas.gameObject.SetActive(true);
        }
    } // end TakeScreenshot

    private void SetTerrainHeights(bool zero = true) {
        if (zero == false && this.terrainData.GetHeight(0, 0) != 0f) return;

        float[,] heights = this.terrainData.GetHeights(0, 0, this.terrainData.heightmapWidth, this.terrainData.heightmapHeight);

        for (int y = 0; y < this.terrainData.heightmapWidth; y++) {
            for (int x = 0; x < this.terrainData.heightmapHeight; x++) {
                heights[y, x] = (zero) ? 0 : this.savedHeights[y, x];
            }
        }

        this.terrainData.SetHeights(0, 0, heights);
    } // end SetTerrainHeight

    public void SetPosition() {
        Texture2D img = GameInfo.Image;

        TerrainData terrainData = Terrain.activeTerrain.terrainData;
        float scaleFactor = terrainData.size.x / terrainData.heightmapResolution;

        float x = (img.height / 2) * scaleFactor;
        float y = 10;
        float z = (img.width / 2) * scaleFactor;
        this.transform.position = new Vector3(x, y, z);

        this.cam.orthographicSize = z;
    } // end SetPosition

} // end Screenshotter
